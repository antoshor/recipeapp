//
//  CategoriesPresenter.swift
//  RecipeApp
//
//  Created by Mac Admin on 04.07.2023.
//

final class CategoriesPresenter {
    
    // MARK: - Public Properties
    weak var view: CategoriesViewControllerProtocol?
    let interactor: CategoriesInteractorProtocol
    
    // MARK: - Initializers
    init(interactor: CategoriesInteractorProtocol) {
        self.interactor = interactor
    }
}

// MARK: - CategoriesPresenterProtocol
extension CategoriesPresenter: CategoriesPresenterProtocol {
    
    // MARK: - viewDidLoaded
    func viewDidLoaded() {
        interactor.loadData()
        interactor.setupNavigationVC()
    }
    
    // MARK: - didLoad
    func didLoad(data: CategoriesModel) {
        view?.showUI(data: data.dishes)
    }
    
    // MARK: - setupNavigationVC
    func setupNavigationVC(with title: String, with profileImage: String) {
        view?.showCategories(title)
        view?.createRightNavigationItem(imageName: profileImage)
    }
    
    // MARK: - didTapToCategory
    func didTapToCategory(index: Int) {
        interactor.getCategory(index: index)
    }
    
    // MARK: - getCurrentCategory
    func getCurrentCategory(dish: Dish) {
        view?.dishTapped(dish: dish)
    }
}
