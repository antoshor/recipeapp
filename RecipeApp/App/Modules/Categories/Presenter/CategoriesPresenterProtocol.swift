//
//  CategoriesPresenterProtocol.swift
//  RecipeApp
//
//  Created by Mac Admin on 04.07.2023.
//

protocol CategoriesPresenterProtocol: AnyObject {
    
    func didLoad(data: CategoriesModel)
    
    func viewDidLoaded()
    
    func setupNavigationVC(with title: String, with profileImage: String)
    
    func didTapToCategory(index: Int)
    
    func getCurrentCategory(dish: Dish) 
}
