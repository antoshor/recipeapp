//
//  CategoriesSection.swift
//  RecipeApp
//
//  Created by Mac Admin on 26.07.2023.
//

// MARK: - CategoriesSection
enum CategoriesSection: Int, CaseIterable {
    case categories
}

// MARK: - CategoriesItem
enum CategoriesItem: Hashable {
    case categoriesInfo(Dish)
}

// MARK: - CategoriesTags
enum CategoryTag: String {
    case allСategories
    case salads
    case rice
    case fish
    case soups
    
    var tag: String {
        switch self {
        case .allСategories:
            return "Все меню"
        case .salads:
            return "Салаты"
        case .rice:
            return "С рисом"
        case .fish:
            return "С рыбой"
        case .soups:
            return "Супы"
        }
    }
    
    var isSelected: Bool {
        switch self {
        case .allСategories:
            return true
        case .salads:
            return false
        case .rice:
            return false
        case .fish:
            return false
        case .soups:
            return false
        }
    }
}
