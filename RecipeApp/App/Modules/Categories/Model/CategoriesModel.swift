//
//  CategoriesModel.swift
//  RecipeApp
//
//  Created by Mac Admin on 26.07.2023.
//

// MARK: - CategoriesModel
struct CategoriesModel: Codable {
    var dishes: [Dish]
}

// MARK: - Dish
struct Dish: Hashable, Codable {
    let id: Int
    let name: String
    var price, weight: Int
    let description: String
    let imageURL: String
    let tegs: [Tag]

    enum CodingKeys: String, CodingKey {
        case id, name, price, weight, description
        case imageURL = "image_url"
        case tegs
    }
}

// MARK: - Tag
enum Tag: String, Codable {
    case all = "Все меню"
    case rice = "С рисом"
    case fish = "С рыбой"
    case salads = "Салаты"
    case soups = "Супы"
}
