//
//  CategoriesViewControllerProtocol.swift
//  RecipeApp
//
//  Created by Mac Admin on 04.07.2023.
//

protocol CategoriesViewControllerProtocol: AnyObject {
    
    func showCategories(_ description: String)
    
    func showUI(data: [Dish])
    
    func dishTapped(dish: Dish)
    
    func createRightNavigationItem(imageName: String)
}
