//
//  CategoriesViewController.swift
//  RecipeApp
//
//  Created by Mac Admin on 04.07.2023.
//

import UIKit

final class CategoriesViewController: UIViewController, CategoriesViewControllerProtocol, CompletionController {
    
    // MARK: - UI Properties
    private lazy var categoriesCollectionView: UICollectionView = {
        let collectionView = UICollectionView(frame: CGRect(x: 0, y: 0, width: 100, height: 100),collectionViewLayout: CategoriesCompositionalLayout.createLayout())
        collectionView.delegate = self
        collectionView.register(CategoriesCollectionViewCell.self, forCellWithReuseIdentifier: CategoriesCollectionViewCell.reuseID)
        
        return collectionView
    }()
    
    // MARK: - Public Properties
    var presenter: CategoriesPresenterProtocol?
    var collectionViewDataSource: CategoriesDataSourceProtocol?
    
    typealias CompletionData = Dish
    var completionHandler: ((Dish) -> ())?
    var completionHandlerBackButtonPressed: (() -> ())?
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        presenter?.viewDidLoaded()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        if isMovingFromParent {
            completionHandlerBackButtonPressed?()
        }
    }
    
    // MARK: - Objc Methods
    @objc func profileItemTapped() {
        print("profileItemTapped")
    }
    
    // MARK: - Public Methods
    func showCategories(_ description: String) {
        navigationItem.title = description
    }
    
    // MARK: - showUI
    func showUI(data: [Dish]) {
        setupConstrains()
        collectionViewDataSource?.configureDataSource(collectionView: categoriesCollectionView, data: data)
    }
    
    // MARK: - dishTapped
    func dishTapped(dish: Dish) {
        completionHandler?(dish)
    }
    
    // MARK: - createRightNavigationItem
    func createRightNavigationItem(imageName: String) {
        let button = UIButton()
        
        button.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        button.layer.cornerRadius = button.frame.height / 2
        button.clipsToBounds = true
        button.setImage(UIImage(named: imageName), for: .normal)
        button.addTarget(self, action: #selector(profileItemTapped), for: .touchUpInside)
        
        let rightBarButton = UIBarButtonItem(customView: UIView(frame: CGRect(x: 0, y: 0, width: 40, height: 40)))
        rightBarButton.customView?.addSubview(button)
        rightBarButton.customView?.frame = button.frame
        
        navigationItem.rightBarButtonItem = rightBarButton
    }
    
    // MARK: - setupConstrains
    private func setupConstrains() {
        categoriesCollectionView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(categoriesCollectionView)
        
        NSLayoutConstraint.activate([
            categoriesCollectionView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            categoriesCollectionView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            categoriesCollectionView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            categoriesCollectionView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor)
        ])
    }
}

// MARK: - UICollectionViewDelegate
extension CategoriesViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        presenter?.didTapToCategory(index: indexPath.row)
    }
}


