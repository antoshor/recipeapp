//
//  CategoriesDataSourceProtocol.swift
//  RecipeApp
//
//  Created by Mac Admin on 03.10.2023.
//

import UIKit

protocol CategoriesDataSourceProtocol: AnyObject, DiffableDataSourceProtocol {
    
    func configureDataSource(collectionView: UICollectionView, data: [Dish])
}
