//
//  CategoriesDiffableDataSource.swift
//  RecipeApp
//
//  Created by Mac Admin on 27.07.2023.
//

import UIKit

final class CategoriesDiffableDataSource: CategoriesDataSourceProtocol {
    
    // MARK: - Private Properties
    private var dataSource: UICollectionViewDiffableDataSource<CategoriesSection, CategoriesItem>?
    
    // MARK: - Public Methods
    func configureDataSource(collectionView: UICollectionView, data: [Dish]) {
        dataSource = UICollectionViewDiffableDataSource<CategoriesSection, CategoriesItem>(collectionView: collectionView, cellProvider:{ [weak self] (collectionView, indexPath, item) -> UICollectionViewCell? in
            
            switch item {
            case .categoriesInfo(let info):
                let cell = self?.createCell(cellType: CategoriesCollectionViewCell.self, indexPath: indexPath, collectionView: collectionView)
                cell?.configure(with: info)
                return cell
            }
        })
        
        createHeader(dish: data)
        reloadData(with: CategoriesModel(dishes: data))
    }
    
    // MARK: - Private Methods
    // MARK: - createHeader
    private func createHeader(dish: [Dish]) {
        let headerRegistration = UICollectionView.SupplementaryRegistration
        <CategoriesSectionHeader>(elementKind: UICollectionView.elementKindSectionHeader) { [weak self] (headerView, elementKind, indexPath) in
            
            headerView.categoriesButtonDidTappedCallback = { categoryTag in
                guard let tag = categoryTag?.tag else {
                    return
                }
                self?.filterDishList(tag: tag, dish: dish)
            }
        }
        
        dataSource?.supplementaryViewProvider = {
            (collectionView, elementKind, indexPath) -> UICollectionReusableView? in
            
            guard let section = CategoriesSection(rawValue: indexPath.section) else {
                return UICollectionReusableView()
            }
            
            switch section {
            case .categories:
                return collectionView.dequeueConfiguredReusableSupplementary(
                    using: headerRegistration, for: indexPath)
            }
        }
    }
    
    // MARK: - filterDishList
    private func filterDishList(tag: String, dish: [Dish]) {
        let items = dish.filter { item in
            guard let currentTag = Tag(rawValue: tag) else {
                return false
            }
            
            guard item.tegs.contains(currentTag) else {
                return false
            }
            
            return true
        }
        
        reloadData(with: CategoriesModel(dishes: items))
    }
    
    // MARK: - reloadData
    private func reloadData(with data: CategoriesModel) {
        var snapshot = NSDiffableDataSourceSnapshot<CategoriesSection, CategoriesItem>()
        snapshot.appendSections([.categories])
        data.dishes.forEach { item in
            snapshot.appendItems([.categoriesInfo(item)], toSection: .categories)
        }
        
        dataSource?.apply(snapshot, animatingDifferences: true)
    }
}
