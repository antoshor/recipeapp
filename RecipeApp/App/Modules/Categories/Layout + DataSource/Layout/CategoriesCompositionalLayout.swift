//
//  CategoriesCompositionalLayout.swift
//  RecipeApp
//
//  Created by Mac Admin on 26.07.2023.
//

import UIKit

final class CategoriesCompositionalLayout: CompositionalLayoutProtocol {
    
    // MARK: - Public Methods
    static func createLayout() -> UICollectionViewLayout {
        
        let layout = UICollectionViewCompositionalLayout  { (sectionIndex, layoutEnvironment) -> NSCollectionLayoutSection? in
            
            guard let sectionKind = CategoriesSection(rawValue: sectionIndex) else {
                return nil
            }
            
            switch sectionKind {
            case .categories:
                let itemSpacing: CGFloat = 8
                let itemSize = NSCollectionLayoutSize(
                    widthDimension: .fractionalWidth(0.33),
                    heightDimension: .fractionalHeight(1.0))
                let item = NSCollectionLayoutItem(layoutSize: itemSize)
                
                let groupSpacing: CGFloat = 13
                let groupSize = NSCollectionLayoutSize(
                    widthDimension: .fractionalWidth(1.0),
                    heightDimension: .fractionalWidth(0.42))
                let group = NSCollectionLayoutGroup.horizontal(layoutSize: groupSize, subitem: item, count: 3)
                group.interItemSpacing = .fixed(itemSpacing)
                
                let sectionSpacing: CGFloat = 16
                let section = NSCollectionLayoutSection(group: group)
                section.contentInsets = .init(top: 0, leading: sectionSpacing, bottom: sectionSpacing, trailing: sectionSpacing)
                section.interGroupSpacing = groupSpacing
                
                let header = CategoriesCompositionalLayout.createSectionHeader()
                section.boundarySupplementaryItems = [header]
                
                return section
            }
        }
        return layout
    }
    
    // MARK: - Private Methods
    private static func createSectionHeader() -> NSCollectionLayoutBoundarySupplementaryItem {
        let layoutSectionHeaderSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1), heightDimension: .estimated(65))
        let layoutSectionHeader = NSCollectionLayoutBoundarySupplementaryItem(layoutSize: layoutSectionHeaderSize, elementKind: UICollectionView.elementKindSectionHeader, alignment: .top)
        
        layoutSectionHeader.pinToVisibleBounds = true
        return layoutSectionHeader
    }
}
