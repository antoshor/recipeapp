//
//  CategoriesCollectionViewCell.swift
//  RecipeApp
//
//  Created by Mac Admin on 26.07.2023.
//

import UIKit

final class CategoriesCollectionViewCell: UICollectionViewCell {
    
    // MARK: - UI Properties
    // MARK: - containerStackView
    private lazy var containerStackView: UIStackView = {
        let stack = UIStackView()
        stack.axis = .vertical
        [imageViewContainer, titleLabel].forEach {
            stack.addArrangedSubview($0)
        }
        
        return stack
    }()
    
    // MARK: - imageViewContainer
    private lazy var imageViewContainer: UIView = {
        let view = UIView()
        view.backgroundColor = #colorLiteral(red: 0.9782709479, green: 0.9749543071, blue: 0.9687969089, alpha: 1)
        view.layer.cornerRadius = 10
        view.addSubview(imageView)
        
        return view
    }()
    
    // MARK: - imageView
    private lazy var imageView: UIImageView = {
        let image = UIImageView()
        image.contentMode = .scaleAspectFit
        
        return image
    }()
    
    // MARK: - titleLabel
    private lazy var titleLabel: UILabel = {
        let title = UILabel()
        title.font = UIFont(name: "SFProDisplay-Regular", size: 14)
        title.numberOfLines = 0
        title.textAlignment = .center
        title.adjustsFontSizeToFitWidth = true
        
        return title
    }()
    
    // MARK: - Private Properties
    private let queue = DispatchQueue(label: "queue.cell")
    private var checkData: Dish?
    
    // MARK: - Initializers
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepareForReuse() {
        imageView.image = nil
        titleLabel.text = nil
    }
    
    // MARK: - Private Methods
    private func setupConstraints() {
        setupStackViewConstraints()
        setupImageViewContainerConstraints()
        setupImageViewConstraints()
    }
    
    // MARK: - setupStackViewConstraints
    private func setupStackViewConstraints() {
        containerStackView.translatesAutoresizingMaskIntoConstraints = false
        addSubview(containerStackView)
        
        NSLayoutConstraint.activate([
            containerStackView.leadingAnchor.constraint(equalTo: leadingAnchor),
            containerStackView.topAnchor.constraint(equalTo: topAnchor),
            containerStackView.trailingAnchor.constraint(equalTo: trailingAnchor),
            containerStackView.bottomAnchor.constraint(equalTo: bottomAnchor),
        ])
    }
    
    // MARK: - setupImageViewContainerConstraints
    private func setupImageViewContainerConstraints() {
        imageViewContainer.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            imageViewContainer.widthAnchor.constraint(equalTo: imageViewContainer.heightAnchor)
        ])
    }
    
    // MARK: - setupImageViewConstraints
    private func setupImageViewConstraints() {
        imageView.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            imageView.centerXAnchor.constraint(equalTo: imageViewContainer.centerXAnchor),
            imageView.centerYAnchor.constraint(equalTo: imageViewContainer.centerYAnchor),
            
            imageView.widthAnchor.constraint(equalTo: imageView.heightAnchor),
            
            imageView.leadingAnchor.constraint(greaterThanOrEqualTo: containerStackView.leadingAnchor, constant: 10),
            imageView.topAnchor.constraint(greaterThanOrEqualTo: containerStackView.topAnchor, constant: 10),
            imageView.trailingAnchor.constraint(lessThanOrEqualTo: containerStackView.trailingAnchor, constant: -10),
            imageView.bottomAnchor.constraint(lessThanOrEqualTo: containerStackView.bottomAnchor, constant: -10)
        ])
    }
}

// MARK: - CollectionViewCellsProtocol
extension CategoriesCollectionViewCell: CollectionViewCellsProtocol {
    
    typealias CellType = Dish
    
    static var reuseID: String {
        return "CategoriesCollectionViewCell"
    }
    
    func configure(with cellData: Dish) {
        checkData = cellData
        queue.async {
            guard let url = URL(string:"\(cellData.imageURL)") else { return }
            if let data = try? Data(contentsOf: url),
               self.checkData == cellData
            {
                DispatchQueue.main.async {
                    self.imageView.image = UIImage(data: data)
                    self.titleLabel.text = cellData.name
                }
            }
        }
    }
    
}
