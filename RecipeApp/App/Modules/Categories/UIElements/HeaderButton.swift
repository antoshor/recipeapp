//
//  HeaderButton.swift
//  RecipeApp
//
//  Created by Mac Admin on 27.09.2023.
//

import UIKit

class HeaderButton: UIButton {
    
    // MARK: - Public Properties
    var buttonTag: CategoryTag?
    
    // MARK: - updateConfiguration
    override func updateConfiguration() {
        guard let configuration = configuration else {
            return
        }
        
        var updatedConfiguration = configuration
        
        var background = UIButton.Configuration.filled().background
        
        let foregroundColor: UIColor
        let backgroundColor: UIColor
        let baseColor = #colorLiteral(red: 0.973, green: 0.969, blue: 0.961, alpha: 1)
        
        switch self.state {
        case .selected, .highlighted:
            foregroundColor = .white
            backgroundColor = #colorLiteral(red: 0.2, green: 0.3921568627, blue: 0.8784313725, alpha: 1)
        default:
            foregroundColor = .black
            backgroundColor = baseColor
        }
        
        background.backgroundColorTransformer = UIConfigurationColorTransformer { color in
            
            return backgroundColor
        }
        
        updatedConfiguration.titleTextAttributesTransformer = UIConfigurationTextAttributesTransformer { incoming in
            
            var container = incoming
            container.foregroundColor = foregroundColor
            
            return container
        }
        
        updatedConfiguration.background = background
        
        self.configuration = updatedConfiguration
    }
}

// MARK: - Extensions
extension UIButton {
    class func createCategoryButton(buttonTag: CategoryTag) -> HeaderButton {
        
        var configuration = HeaderButton.Configuration.filled()
        
        configuration.titleAlignment = .center
        configuration.cornerStyle = .large
        configuration.contentInsets = NSDirectionalEdgeInsets(top: CGFloat(10), leading: CGFloat(16), bottom: CGFloat(10), trailing: CGFloat(16))
        
        var container = AttributeContainer()
        container.font = UIFont(name: "SFProDisplay-Medium", size: 14)
        configuration.attributedTitle = AttributedString(buttonTag.tag, attributes: container)
        
        let button = HeaderButton(configuration: configuration, primaryAction: nil)
        button.isSelected = buttonTag.isSelected
        button.buttonTag = buttonTag
        
        button.titleLabel?.numberOfLines = 1
        
        return button
    }
}
