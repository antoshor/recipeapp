//
//  CategoriesSectionHeader.swift
//  RecipeApp
//
//  Created by Mac Admin on 04.08.2023.
//

import UIKit

// MARK: - CategoriesSectionHeader
class CategoriesSectionHeader: UICollectionReusableView {
    
    // MARK: - UI Properties
    // MARK: - categoriesStackView
    private lazy var categoriesStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.distribution = .fillProportionally
        stackView.spacing = 10
        
        [allСategoriesButton, saladsButton, riceButton, fishButton, soupsButton].forEach {
            
            $0.addTarget(self, action: #selector(categoriesDidTapped), for: .touchUpInside)
            
            stackView.addArrangedSubview($0)
        }
        
        return stackView
    }()
    
    // MARK: - categoriesScrollView
    private lazy var categoriesScrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.bounces = true
        scrollView.showsHorizontalScrollIndicator = false
        scrollView.showsVerticalScrollIndicator = false
        scrollView.addSubview(categoriesStackView)
        
        return scrollView
    }()
    
    // MARK: - allСategoriesButton
    private lazy var allСategoriesButton: UIButton = {
        let button = UIButton.createCategoryButton(buttonTag: .allСategories)
        selectedCategory = button
        
        return button
    }()
    
    // MARK: - saladsButton
    private lazy var saladsButton: UIButton = {
        let button = UIButton.createCategoryButton(buttonTag: .salads)
        
        return button
    }()
    
    // MARK: - riceButton
    private lazy var riceButton: UIButton = {
        let button = UIButton.createCategoryButton(buttonTag: .rice)
        
        return button
    }()
    
    // MARK: - fishButton
    private lazy var fishButton: UIButton = {
        let button = UIButton.createCategoryButton(buttonTag: .fish)
        
        return button
    }()
    
    // MARK: - rollsButton
    private lazy var soupsButton: UIButton = {
        let button = UIButton.createCategoryButton(buttonTag: .soups)
        
        return button
    }()
    
    // MARK: - Public Properties
    var categoriesButtonDidTappedCallback: ((_ tag: CategoryTag?) -> ())?
    static let reuseID = "SectionHeader"
    
    // MARK: - Private Properties
    private var selectedCategory: UIButton?
    
    // MARK: - Lifecycle
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .white
        setupConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Objc Methods
    @objc func categoriesDidTapped(_ sender: UIButton) {
        guard let button = sender as? HeaderButton else {
            return
        }
        selectedCategory?.isSelected = false
        button.isSelected = true
        selectedCategory = button
        categoriesButtonDidTappedCallback?(button.buttonTag)
    }
    
    // MARK: - Private Methods
    // MARK: - setupConstraints
    private func setupConstraints() {
        setupStackViewConstraints()
        setupScrollViewConstraints()
    }
    
    // MARK: - setupStackViewConstraints
    private func setupStackViewConstraints() {
        categoriesScrollView.translatesAutoresizingMaskIntoConstraints = false
        addSubview(categoriesScrollView)
        
        NSLayoutConstraint.activate([
            categoriesScrollView.topAnchor.constraint(equalTo: topAnchor),
            categoriesScrollView.leadingAnchor.constraint(equalTo: leadingAnchor),
            categoriesScrollView.trailingAnchor.constraint(equalTo: trailingAnchor),
            categoriesScrollView.bottomAnchor.constraint(equalTo: bottomAnchor)
        ])
    }
    
    // MARK: - setupScrollViewConstraints
    private func setupScrollViewConstraints() {
        categoriesStackView.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            categoriesStackView.topAnchor.constraint(equalTo: categoriesScrollView.topAnchor, constant: 13),
            categoriesStackView.leadingAnchor.constraint(equalTo: categoriesScrollView.leadingAnchor),
            categoriesStackView.trailingAnchor.constraint(equalTo: categoriesScrollView.trailingAnchor),
            categoriesStackView.bottomAnchor.constraint(equalTo: categoriesScrollView.bottomAnchor)
        ])
    }
}
