//
//  CategoriesInteractor.swift
//  RecipeApp
//
//  Created by Mac Admin on 04.07.2023.
//

final class CategoriesInteractor {
    
    // MARK: - Public Properties
    weak var presenter: CategoriesPresenterProtocol?
    let categoryTitle: String
    
    // MARK: - Private Properties
    var data = CategoriesModel(dishes: [Dish]())
    
    // MARK: - Lifecycle
    init(categoryTitle: String) {
        self.categoryTitle = categoryTitle
    }
}

// MARK: - CategoriesInteractorProtocol
extension CategoriesInteractor: CategoriesInteractorProtocol {
    // MARK: - loadData
    func loadData() {
        NetworkService.shared.loadData(url: Api.categories.rawValue) { [weak self] (data: CategoriesModel) in
            self?.data = data
            self?.presenter?.didLoad(data: data)
        }
    }
    
    // MARK: - setupNavigationVC
    func setupNavigationVC() {
        presenter?.setupNavigationVC(with: categoryTitle, with: ImageItems.profile.rawValue)
    }
    
    // MARK: - getCategory
    func getCategory(index: Int) {
        presenter?.getCurrentCategory(dish: data.dishes[index])
    }
}
