//
//  CategoriesInteractorProtocol.swift
//  RecipeApp
//
//  Created by Mac Admin on 04.07.2023.
//

protocol CategoriesInteractorProtocol: AnyObject {
 
    func loadData()
    
    func getCategory(index: Int)
    
    func setupNavigationVC()
    
    var data: CategoriesModel { get set }
}
