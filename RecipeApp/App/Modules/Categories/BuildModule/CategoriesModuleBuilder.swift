//
//  CategoriesModuleBuilder.swift
//  RecipeApp
//
//  Created by Mac Admin on 04.07.2023.
//

import UIKit

final class CategoriesModuleBuilder {
    static func build(categoryTitle: String) -> CategoriesViewController {
        
        let interactor = CategoriesInteractor(categoryTitle: categoryTitle)
        
        let presenter = CategoriesPresenter(interactor: interactor)
        
        let vc = CategoriesViewController()
        vc.presenter = presenter
        vc.collectionViewDataSource = CategoriesDiffableDataSource()
       
        presenter.view = vc
        
        interactor.presenter = presenter
    
        return vc
    }
}

