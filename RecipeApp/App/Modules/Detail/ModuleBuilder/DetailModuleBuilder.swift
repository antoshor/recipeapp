//
//  DetailModuleBuilder.swift
//  RecipeApp
//
//  Created by Mac Admin on 27.08.2023.
//

final class DetailModuleBuilder {
    
    static func build(data: Dish?) -> DetailViewController {
        
        let interactor = DetailInteractor(data: data)
        
        let viewController = DetailViewController()
        
        let presenter = DetailPresenter(interactor: interactor)
        presenter.view = viewController
         
        viewController.presenter = presenter
        
        interactor.presenter = presenter
    
        return viewController
    }
}
