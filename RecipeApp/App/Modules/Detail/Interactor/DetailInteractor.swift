//
//  DetailInteractor.swift
//  RecipeApp
//
//  Created by Mac Admin on 27.08.2023.
//

final class DetailInteractor {
    
    // MARK: - Public Properties
    weak var presenter: DetailPresenterProtocol?
    var data: Dish?
    
    // MARK: - Initializers
    init(data: Dish?) {
        self.data = data
    }
}

// MARK: - DetailInteractorProtocol
extension DetailInteractor: DetailInteractorProtocol {
    
    func getData() {
        guard let data = data else {
            return
        }
        presenter?.didLoadData(data: data)
    }
}
