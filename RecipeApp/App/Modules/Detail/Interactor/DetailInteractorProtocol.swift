//
//  DetailInteractorProtocol.swift
//  RecipeApp
//
//  Created by Mac Admin on 27.08.2023.
//

protocol DetailInteractorProtocol: AnyObject {
    
    var data: Dish? { get set }
    
    func getData()
}
