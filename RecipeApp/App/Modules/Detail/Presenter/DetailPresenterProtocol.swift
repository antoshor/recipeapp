//
//  DetailPresenterProtocol.swift
//  RecipeApp
//
//  Created by Mac Admin on 27.08.2023.
//

protocol DetailPresenterProtocol: AnyObject {
    
    func initialLoad()
    
    func didLoadData(data: Dish)
    
    func createOrder()
    
    func dismiss()
}
