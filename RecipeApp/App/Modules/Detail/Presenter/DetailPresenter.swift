//
//  DetailPresenter.swift
//  RecipeApp
//
//  Created by Mac Admin on 27.08.2023.
//
import UIKit

final class DetailPresenter {
    
    // MARK: - Public Properties
    weak var view: DetailViewProtocol?
    var interactor: DetailInteractorProtocol
    
    // MARK: - Initializers
    init(interactor: DetailInteractorProtocol) {
        self.interactor = interactor
    }
}

// MARK: - DetailPresenterProtocol
extension DetailPresenter: DetailPresenterProtocol {
    
    // MARK: - initialLoad
    func initialLoad() {
        interactor.getData()
    }
    
    // MARK: - didLoadData
    func didLoadData(data: Dish) {
        DispatchQueue.global().async {
            var image = UIImage(named: "notFound")
            if let url = URL(string: data.imageURL) {
                image = try? UIImage(data: Data(contentsOf: url))
            }
            
            DispatchQueue.main.async {
                self.view?.showUI(with: data, image: image!)
            }
        }
    }
    
    // MARK: - createOrder
    func createOrder() {
        view?.dismiss(shoppingCart: ShoppingCartModel(dish: interactor.data!, count: 1))
    }
    
    // MARK: - dismiss
    func dismiss() {
        view?.dismiss(shoppingCart: nil)
    }
}
