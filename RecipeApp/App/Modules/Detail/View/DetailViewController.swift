//
//  DetailViewController.swift
//  RecipeApp
//
//  Created by Mac Admin on 27.08.2023.
//

import UIKit

final class DetailViewController: UIViewController, DetailViewProtocol, CompletionController {
    
    // MARK: - UI Properties
    // MARK: - mainContainerView
    private lazy var mainContainerView: UIView = {
        let containerView = UIView()
        containerView.layer.cornerRadius = 15
        containerView.backgroundColor = .white
        containerView.addSubview(mainContainerStackView)
        containerView.addSubview(controlItemsStackView)
       
        return containerView
    }()
    
    // MARK: - containerImageView
    private lazy var containerImageView: UIView = {
        let containerView = UIView()
        containerView.layer.cornerRadius = 10
        containerView.backgroundColor = #colorLiteral(red: 0.9782709479, green: 0.9749543071, blue: 0.9687969089, alpha: 1)
        containerView.addSubview(imageView)
    
        return containerView
    }()
    
    // MARK: - mainContainerStackView
    private lazy var mainContainerStackView: UIStackView = {
        let stack = UIStackView()
        stack.axis = .vertical
        stack.spacing = 16
        [informationStackView, orderButton].forEach {
            stack.addArrangedSubview($0)
        }
        
        return stack
    }()
    
    // MARK: - costWeightStackView
    private lazy var costWeightStackView: UIStackView = {
        let stack = UIStackView()
        stack.axis = .horizontal
        stack.spacing = 4
        [costLabel, weightLabel].forEach {
            stack.addArrangedSubview($0)
        }
        
        return stack
    }()
    
    // MARK: - imageView
    private lazy var imageView: UIImageView = {
        let image = UIImageView()
        image.contentMode = .scaleAspectFit
        image.clipsToBounds = true
        
        return image
    }()
    
    // MARK: - titleLabel
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 1
        label.adjustsFontSizeToFitWidth = true
        label.font = UIFont(name: "SFProDisplay-Medium", size: 16)
        
        return label
    }()
    
    // MARK: - costLabel
    private lazy var costLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 1
        label.adjustsFontSizeToFitWidth = true
        label.font = UIFont(name: "SFProDisplay-Regular", size: 14)
        
        return label
    }()
    
    // MARK: - weightLabel
    private lazy var weightLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 1
        label.adjustsFontSizeToFitWidth = true
        label.font = UIFont(name: "SFProDisplay-Regular", size: 14)
        label.alpha = 0.35
        
        return label
    }()
    
    // MARK: - descriptionLabel
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.adjustsFontSizeToFitWidth = true
        label.font = UIFont(name: "SFProDisplay-Regular", size: 14)
        label.alpha = 0.65
        
        return label
    }()
    
    // MARK: - informationStackView
    private lazy var informationStackView: UIStackView = {
       let stack = UIStackView()
        stack.axis = .vertical
        stack.spacing = 8
        [containerImageView, titleLabel, costWeightStackView, descriptionLabel].forEach {
            stack.addArrangedSubview($0)
        }
        
        return stack
    }()
    
    // MARK: - orderButton
    private lazy var orderButton: UIButton = {
        var configuration = UIButton.Configuration.filled()
        configuration.cornerStyle = .large
        configuration.buttonSize = .large
        configuration.baseBackgroundColor = #colorLiteral(red: 0.2, green: 0.3921568627, blue: 0.8784313725, alpha: 1)
        
        var container = AttributeContainer()
        container.font = UIFont(name: "SFProDisplay-Regular", size: 16)
        configuration.attributedTitle = AttributedString("Добавить в корзину", attributes: container)
        
        
        let button = UIButton(configuration: configuration, primaryAction: nil)
        button.addTarget(self, action: #selector(orderButtonTapped), for: .touchUpInside)
        
        return button
    }()
    
    // MARK: - controlItemsStackView
    private lazy var controlItemsStackView: UIStackView = {
        let stack = UIStackView()
        stack.axis = .horizontal
        stack.spacing = 8
        stack.distribution = .fillEqually
        [favoritesButton, dismissButton].forEach {
            stack.addArrangedSubview($0)
        }
        
        return stack
    }()
    
    // MARK: - favoritesButton
    private lazy var favoritesButton: UIButton = {
        var configuration = UIButton.Configuration.filled()
        configuration.cornerStyle = .large
        configuration.buttonSize = .large
        configuration.image = UIImage(named: "favorites")
        configuration.baseBackgroundColor = .white
        
        let button = UIButton(configuration: configuration, primaryAction: nil)
        button.addTarget(self, action: #selector(favoritesButtonTapped), for: .touchUpInside)
        
        return button
    }()
    
    // MARK: - dismissButton
    private lazy var dismissButton: UIButton = {
        var configuration = UIButton.Configuration.filled()
        configuration.cornerStyle = .large
        configuration.buttonSize = .large
        configuration.image = UIImage(named: "dismiss")
        configuration.baseBackgroundColor = .white
        
        let button = UIButton(configuration: configuration, primaryAction: nil)
        button.addTarget(self, action: #selector(dismissButtonTapped), for: .touchUpInside)
        
        return button
    }()
    
    // MARK: - Public Properties
    var presenter: DetailPresenterProtocol?
    var completionHandler: ((ShoppingCartModel?) -> ())?
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
        presenter?.initialLoad()
    }
    
    // MARK: - Objc Methods
    @objc func orderButtonTapped() {
        presenter?.createOrder()
    }
    
    @objc func dismissButtonTapped() {
        presenter?.dismiss()
    }
    
    @objc func favoritesButtonTapped() {
       print("favoritesButtonTapped")
    }
    
    // MARK: - Public Methods
    // MARK: - showUI
    func showUI(with: Dish, image: UIImage) {
        imageView.image = image
        titleLabel.text = with.name
        costLabel.text = "\(with.price) Р"
        weightLabel.text = "\(with.weight)г"
        descriptionLabel.text = with.description
        setupConstraints()
    }
    
    // MARK: - dismiss
    func dismiss(shoppingCart: ShoppingCartModel?) {     completionHandler?(shoppingCart)
    }
    
    // MARK: - Private Methods
    // MARK: - configureView
    private func configureView() {
        view.backgroundColor = .black.withAlphaComponent(0.6)
    }
    
    // MARK: - setupConstraints
    private func setupConstraints() {
        setupMainContainerView()
        setupMainContainerStackView()
        setupContainerImageView()
        setupImageView()
        setupPriority()
        setupControlItemsStackView()
    }
    
    // MARK: - setupMainContainerView
    private func setupMainContainerView() {
        mainContainerView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(mainContainerView)
        
        NSLayoutConstraint.activate([
            mainContainerView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 16),
            mainContainerView.topAnchor.constraint(greaterThanOrEqualTo: view.safeAreaLayoutGuide.topAnchor, constant: 16),
            mainContainerView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -16),
            mainContainerView.bottomAnchor.constraint(lessThanOrEqualTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -16),
            
            mainContainerView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            mainContainerView.centerYAnchor.constraint(equalTo: view.centerYAnchor)
        ])
    }
    
    // MARK: - setupMainContainerStackView
    private func setupMainContainerStackView() {
        mainContainerStackView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            mainContainerStackView.topAnchor.constraint(equalTo: mainContainerView.topAnchor, constant: 16),
            mainContainerStackView.leadingAnchor.constraint(equalTo: mainContainerView.leadingAnchor, constant: 16),
            mainContainerStackView.trailingAnchor.constraint(equalTo: mainContainerView.trailingAnchor, constant: -16),
            mainContainerStackView.bottomAnchor.constraint(equalTo: mainContainerView.bottomAnchor, constant: -16),
        ])
    }
    
    // MARK: - setupContainerImageView
    private func setupContainerImageView() {
        containerImageView.translatesAutoresizingMaskIntoConstraints = false
        let containerImageViewWidthConstraint = containerImageView.widthAnchor.constraint(equalTo: containerImageView.heightAnchor, multiplier: 100/74)
        containerImageViewWidthConstraint.priority = .defaultHigh
        
        NSLayoutConstraint.activate([
            containerImageViewWidthConstraint
        ])
    }
    
    // MARK: - setupImageView
    private func setupImageView() {
        imageView.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            imageView.topAnchor.constraint(greaterThanOrEqualTo: containerImageView.topAnchor, constant: 16),
            imageView.leadingAnchor.constraint(greaterThanOrEqualTo: containerImageView.leadingAnchor, constant: 16),
            imageView.trailingAnchor.constraint(lessThanOrEqualTo: containerImageView.trailingAnchor, constant: -16),
            imageView.bottomAnchor.constraint(lessThanOrEqualTo: containerImageView.bottomAnchor, constant: -16),
            
            imageView.heightAnchor.constraint(equalTo: imageView.widthAnchor, multiplier: 1),
            
            imageView.centerXAnchor.constraint(equalTo: containerImageView.centerXAnchor),
            imageView.centerYAnchor.constraint(equalTo: containerImageView.centerYAnchor)
        ])
    }
    
    // MARK: - setupPriority
    private func setupPriority() {
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        costLabel.translatesAutoresizingMaskIntoConstraints = false
        weightLabel.translatesAutoresizingMaskIntoConstraints = false
        descriptionLabel.translatesAutoresizingMaskIntoConstraints = false
        orderButton.translatesAutoresizingMaskIntoConstraints = false
        
        titleLabel.setContentCompressionResistancePriority(UILayoutPriority(rawValue: 752), for: .vertical)
        costLabel.setContentCompressionResistancePriority(UILayoutPriority(rawValue: 751), for: .vertical)
        weightLabel.setContentCompressionResistancePriority(UILayoutPriority(rawValue: 750), for: .vertical)
        weightLabel.setContentHuggingPriority(UILayoutPriority(rawValue: 249), for: .horizontal)
        descriptionLabel.setContentCompressionResistancePriority(UILayoutPriority(751), for: .vertical)
        orderButton.setContentCompressionResistancePriority(UILayoutPriority(rawValue: 752), for: .vertical)
    }
    
    // MARK: - setupControlItemsStackView
    private func setupControlItemsStackView() {
        controlItemsStackView.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            controlItemsStackView.topAnchor.constraint(greaterThanOrEqualTo: mainContainerView.topAnchor, constant: 24),
            controlItemsStackView.trailingAnchor.constraint(lessThanOrEqualTo: mainContainerView.trailingAnchor, constant: -24)
            
        ])
        
        setupFavoritesButton()
    }
    
    // MARK: - setupFavoritesButton
    private func setupFavoritesButton() {
        favoritesButton.translatesAutoresizingMaskIntoConstraints = false
        
        let favoritesHeight =   favoritesButton.heightAnchor.constraint(equalTo: view.heightAnchor, multiplier: 0.05)
        favoritesHeight.priority = .defaultHigh
        
        NSLayoutConstraint.activate([
            favoritesHeight,
            favoritesButton.widthAnchor.constraint(equalTo: dismissButton.widthAnchor, multiplier: 1),
            favoritesButton.widthAnchor.constraint(equalTo: favoritesButton.heightAnchor, multiplier: 1)
        ])
    }
}

