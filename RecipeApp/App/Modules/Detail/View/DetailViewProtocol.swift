//
//  DetailViewProtocol.swift
//  RecipeApp
//
//  Created by Mac Admin on 27.08.2023.
//

import UIKit

protocol DetailViewProtocol: AnyObject {
    
    func showUI(with: Dish, image: UIImage)
 
    func dismiss(shoppingCart: ShoppingCartModel?)
}
