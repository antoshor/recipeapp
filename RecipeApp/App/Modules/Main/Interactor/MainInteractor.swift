//
//  MainInteractor.swift
//  RecipeApp
//
//  Created by Mac Admin on 04.07.2023.
//

import Foundation

final class MainInteractor {
    
    // MARK: - Public Properties
    weak var presenter: MainPresenterProtocol?
    var categories = [Int: String]()
}

// MARK: - MainInteractorProtocol
extension MainInteractor: MainInteractorProtocol {
    
    // MARK: - loadData
    func loadData() {
        var keyCategories = 0
        NetworkService.shared.loadData(url: Api.main.rawValue) { [weak self] (data: MainModel) in
            
            for item in data.сategories {
                self?.categories[keyCategories] = item.name
                keyCategories += 1
            }
            
            self?.presenter?.didLoadData(data)
        }
    }
    
    // MARK: - getNowDate
    func getNowDate() {
        let now = Date()
        let formatter = DateFormatter()
        formatter.locale = Locale(identifier: "ru_RU")
        formatter.setLocalizedDateFormatFromTemplate("dd MMMM")
        let dateString = formatter.string(from: now)
        presenter?.setupNavigationVC(with: dateString)
    }
}
