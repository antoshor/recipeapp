//
//  MainInteractorProtocol.swift
//  RecipeApp
//
//  Created by Mac Admin on 04.07.2023.
//

protocol MainInteractorProtocol: AnyObject {
    
    func loadData()
    
    func getNowDate() 
    
    var categories: [Int: String] { get set }
}
