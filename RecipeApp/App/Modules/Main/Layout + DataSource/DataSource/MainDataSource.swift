//
//  MainDataSource.swift
//  RecipeApp
//
//  Created by Mac Admin on 13.07.2023.
//

import UIKit

// MARK: - MainDataSource
final class MainDiffableDataSource: MainDiffableDataSourceProtocol {
    
    // MARK: - Private Properties
    private var dataSource: UICollectionViewDiffableDataSource<MainSection, MainRow>?
    private var data: MainModel?
    
    // MARK: - Private Methods
    // MARK: - configureDataSource
    func configureDataSource(collectionView: UICollectionView, with data: MainModel) {
        self.data = data
        dataSource = UICollectionViewDiffableDataSource<MainSection, MainRow>(collectionView: collectionView, cellProvider: { (collectionView, indexPath, item) -> UICollectionViewCell? in
        
            switch item {
            case .mainInfo(let info):
                 let cell = self.createCell(cellType: MainCollectionViewCell.self, indexPath: indexPath, collectionView: collectionView)
                cell.configure(with: info)
                return cell
            }
        })
        
        reloadData()
    }
    
    // MARK: - reloadData
    private func reloadData() {
        var snapshot = NSDiffableDataSourceSnapshot<MainSection, MainRow>()
        snapshot.appendSections([.main])
        data?.сategories.forEach {
            snapshot.appendItems([.mainInfo($0)], toSection: .main)
        }
        dataSource?.apply(snapshot)
    }
}
