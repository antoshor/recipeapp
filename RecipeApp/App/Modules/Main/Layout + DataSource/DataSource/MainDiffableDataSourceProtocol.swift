//
//  MainDiffableDataSourceProtocol.swift
//  RecipeApp
//
//  Created by Mac Admin on 03.10.2023.
//

import UIKit


protocol MainDiffableDataSourceProtocol: DiffableDataSourceProtocol {
    
    func configureDataSource(collectionView: UICollectionView, with data: MainModel)
}

