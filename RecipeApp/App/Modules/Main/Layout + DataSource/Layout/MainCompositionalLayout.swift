//
//  MainCompositionalLayout.swift
//  RecipeApp
//
//  Created by Mac Admin on 10.07.2023.
//

import UIKit

final class MainCompositionalLayout: CompositionalLayoutProtocol {
    
    static func createLayout() -> UICollectionViewLayout {
        
        let layout = UICollectionViewCompositionalLayout  { (sectionIndex, layoutEnvironment) -> NSCollectionLayoutSection? in
         
            guard let sectionKind = MainSection(rawValue: sectionIndex) else {
                return nil
            }
            
            switch sectionKind {
            case .main:
                let sectionSpacing: CGFloat = 16
                let groupSpacing: CGFloat = 8
                let itemSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0), heightDimension: .fractionalHeight(1.0))
                let item = NSCollectionLayoutItem(layoutSize: itemSize)
                
                let groupSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0), heightDimension: .fractionalHeight(0.25))
                let group = NSCollectionLayoutGroup.horizontal(layoutSize: groupSize, subitems: [item])
                
                let section = NSCollectionLayoutSection(group: group)
                section.interGroupSpacing = groupSpacing
               
                section.contentInsets = .init(top: groupSpacing, leading: sectionSpacing, bottom: groupSpacing, trailing: sectionSpacing)
                
                return section
            }
        }
        return layout
    }
}

