//
//  MainCollectionViewCell.swift
//  RecipeApp
//
//  Created by Mac Admin on 08.07.2023.
//

import UIKit

final class MainCollectionViewCell: UICollectionViewCell {
    
    // MARK: - UI Properties
    private lazy var imageView: UIImageView = {
        let image = UIImageView()
        image.contentMode = .scaleAspectFill
        image.layer.cornerRadius = 10
        image.clipsToBounds = true
        
        return image
    }()
    
    // MARK: - titleLabel
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "SFProDisplay-Medium", size: 20)
        label.numberOfLines = 1
        
        return label
    }()
    
    // MARK: - Private Properties
    private let queue = DispatchQueue(label: "queue.cell")
    private var checkData: MainItem?
    
    // MARK: - Initializers
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepareForReuse() {
        imageView.image = nil
        titleLabel.text = nil
    }
    
    // MARK: - Private Methods
    // MARK: - setupConstraints
    private func setupConstraints() {
        setupImageViewConstraints()
        setupTitleLabelConstraints()
    }
    
    // MARK: - setupImageViewConstraints
    private func setupImageViewConstraints() {
        imageView.translatesAutoresizingMaskIntoConstraints = false
        addSubview(imageView)
        
        NSLayoutConstraint.activate([
            imageView.leadingAnchor.constraint(equalTo: leadingAnchor),
            imageView.topAnchor.constraint(equalTo: topAnchor),
            imageView.trailingAnchor.constraint(equalTo: trailingAnchor),
            imageView.bottomAnchor.constraint(equalTo: bottomAnchor)
        ])
    }
    
    // MARK: - setupTitleLabelConstraints
    private func setupTitleLabelConstraints() {
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        addSubview(titleLabel)
        
        NSLayoutConstraint.activate([
            titleLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16),
            titleLabel.topAnchor.constraint(equalTo: topAnchor, constant: 16),
            
        ])
    }
}

// MARK: - CollectionViewCellsProtocol
extension MainCollectionViewCell: CollectionViewCellsProtocol {
    
    typealias CellType = MainItem
    
    static var reuseID: String {
        return "MainCollectionViewCell"
    }
    
    func configure(with cellData: MainItem) {
        checkData = cellData
        queue.async {
            guard let url = URL(string:"\(cellData.imageURL)") else { return }
            if let data = try? Data(contentsOf: url),
               self.checkData == cellData
            {
                DispatchQueue.main.async {
                    self.imageView.image = UIImage(data: data)
                    self.titleLabel.text = cellData.name
                }
            }
        }
    }
}
