//
//  MainViewController.swift
//  RecipeApp
//
//  Created by Mac Admin on 04.07.2023.
//

import UIKit

final class MainViewController: UIViewController, MainViewControllerProtocol, CompletionController {
    
    // MARK: - UI Properties
    // MARK: - mainCollectionView
    private lazy var mainCollectionView: UICollectionView = {
        let collectionView = UICollectionView(frame: CGRect(x: 0, y: 0, width: 100, height: 100),collectionViewLayout: MainCompositionalLayout.createLayout())
        collectionView.register(MainCollectionViewCell.self, forCellWithReuseIdentifier: MainCollectionViewCell.reuseID)
        collectionView.delegate = self
        
        return collectionView
    }()

    // MARK: - Public Properties
    typealias CompletionData = String
    var completionHandler: ((String) -> ())?
    var goToNewVcHandler: (() -> ())?
    
    var presenter: MainPresenterProtocol?
    var collectionViewDataSource: MainDiffableDataSourceProtocol?
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        presenter?.initialLoad()
    }
    
    // MARK: - Objc Methods
    @objc func addButtonTapped() {
        goToNewVcHandler?()
        
    }
    
    // MARK: - Objc Methods
    @objc func locationItemTapped() {
        print("locationItemTapped")
    }
    
    @objc func profileItemTapped() {
        print("profileItemTapped")
    }
    
    // MARK: - Public Methods
    func showUI(with data: MainModel) {
        setupConstraints()
        collectionViewDataSource?.configureDataSource(collectionView: mainCollectionView, with: data)
    }
    
    // MARK: - categoryTapped
    func categoryTapped(category: String) {
        completionHandler?(category)
    }
    
    // MARK: - createLeftNavigationItem
    func createLeftNavigationItem(titleLocation: String, subtitleDate: String, imageName: String) {
        var configuration = UIButton.Configuration.plain()
        configuration.title = titleLocation
        configuration.baseForegroundColor = .black
        configuration.subtitle = subtitleDate
        configuration.image = UIImage(named: imageName)
        configuration.imagePadding = 5
        
        let button = UIButton(configuration: configuration, primaryAction: nil)
        button.addTarget(self, action: #selector(locationItemTapped), for: .touchUpInside)
        
        let leftBarButton = UIBarButtonItem.init(customView: button)
        navigationItem.leftBarButtonItem = leftBarButton
    }
    
    // MARK: - createRightNavigationItem
    func createRightNavigationItem(imageName: String) {
        let button = UIButton()
        button.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        button.layer.cornerRadius = button.frame.height / 2
        button.clipsToBounds = true
        button.setImage(UIImage(named: imageName), for: .normal)
        button.addTarget(self, action: #selector(profileItemTapped), for: .touchUpInside)
        
        let rightBarButton = UIBarButtonItem(customView: UIView(frame: CGRect(x: 0, y: 0, width: 40, height: 40)))
        rightBarButton.customView?.addSubview(button)
        rightBarButton.customView?.frame = button.frame
        
        navigationItem.rightBarButtonItem = rightBarButton
    }
    
    // MARK: - Private Methods
    // MARK: - setupConstraints
    private func setupConstraints() {
        mainCollectionView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(mainCollectionView)
        
        NSLayoutConstraint.activate([
            mainCollectionView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            mainCollectionView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            mainCollectionView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            mainCollectionView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor)
        ])
        setupBackButton()
    }
    
    // MARK: - setupBackButton
    private func setupBackButton() {
        navigationItem.backButtonTitle = ""
        navigationController?.navigationBar.tintColor = .black
    }
}

// MARK: - UICollectionViewDelegate
extension MainViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        presenter?.didTapToCategory(to: indexPath.row)
    }
}

