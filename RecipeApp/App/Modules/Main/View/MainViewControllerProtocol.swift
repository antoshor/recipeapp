//
//  MainViewControllerProtocol.swift
//  RecipeApp
//
//  Created by Mac Admin on 04.07.2023.
//

protocol MainViewControllerProtocol: AnyObject {
    
    func showUI(with data: MainModel)

    func createLeftNavigationItem(titleLocation: String, subtitleDate: String, imageName: String)
    
    func createRightNavigationItem(imageName: String)
    
    func categoryTapped(category: String)
}
