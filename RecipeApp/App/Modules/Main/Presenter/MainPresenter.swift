//
//  MainPresenter.swift
//  RecipeApp
//
//  Created by Mac Admin on 04.07.2023.
//

final class MainPresenter {
    
    // MARK: - Public Properties
    weak var view: MainViewControllerProtocol?
    let interactor: MainInteractorProtocol 
    
    // MARK: - Lifecycle
    init(interactor: MainInteractorProtocol) {
        self.interactor = interactor
    }
}

// MARK: - MainPresenterProtocol
extension MainPresenter: MainPresenterProtocol {
   
    // MARK: - initialLoad
    func initialLoad() {
        interactor.loadData()
        interactor.getNowDate()
    }
    
    // MARK: - didLoadData
    func didLoadData(_ data: MainModel?) {
        guard let data = data else {
            print("No initial Data")
            return
        }
        
        view?.showUI(with: data)
    }
    
    // MARK: - setupNavigationVC
    func setupNavigationVC(with date: String) {
        view?.createLeftNavigationItem(titleLocation: Location.moscow.rawValue, subtitleDate: date, imageName: ImageItems.location.rawValue)
        view?.createRightNavigationItem(imageName: ImageItems.profile.rawValue)
    }
    
    // MARK: - didTapToCategory
    func didTapToCategory(to number: Int) {
        guard let category = interactor.categories[number] else {
            print("Error category number \(number) not found")
            return
        }
        view?.categoryTapped(category: category)
    }
}


