//
//  MainPresenterProtocol.swift
//  RecipeApp
//
//  Created by Mac Admin on 04.07.2023.
//

protocol MainPresenterProtocol: AnyObject {
    func initialLoad()
    
    func didLoadData(_ data: MainModel?)
    
    func setupNavigationVC(with date: String)
    
    func didTapToCategory(to number: Int)
}
