//
//  MainModel.swift
//  RecipeApp
//
//  Created by Mac Admin on 04.07.2023.
//

// MARK: - MainModel
struct MainModel: Codable {
    let сategories: [MainItem]
}

// MARK: - MainItem
struct MainItem: Codable, Hashable {
    let id: Int
    let name: String
    let imageURL: String

    enum CodingKeys: String, CodingKey {
        case id, name
        case imageURL = "image_url"
    }
}
