//
//  Enums.swift
//  RecipeApp
//
//  Created by Mac Admin on 24.07.2023.
//

// MARK: - MainSectionKind
enum MainSection: Int, CaseIterable {
    case main
}

// MARK: - MainRow
enum MainRow: Hashable {
    case mainInfo(MainItem)
}
