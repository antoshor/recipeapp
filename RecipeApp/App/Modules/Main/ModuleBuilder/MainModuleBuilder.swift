//
//  MainModuleBuilder.swift
//  RecipeApp
//
//  Created by Mac Admin on 04.07.2023.
//

final class MainModuleBuilder {
    static func build() -> MainViewController {
        let interactor = MainInteractor()
        
        let viewController = MainViewController()
        
        let presenter = MainPresenter(interactor: interactor)
        presenter.view = viewController
        
        viewController.presenter = presenter
        viewController.collectionViewDataSource = MainDiffableDataSource()
        
        interactor.presenter = presenter
        
        return viewController
    }
}
