//
//  ShoppingCartInfoCell.swift
//  RecipeApp
//
//  Created by Mac Admin on 24.08.2023.
//

import UIKit

class ShoppingCartInfoCell: UICollectionViewCell {
    
    // MARK: - UI Properties
    // MARK: - containerView
    private lazy var containerView: UIView = {
        let view = UIView()
        view.backgroundColor = #colorLiteral(red: 0.9725490196, green: 0.968627451, blue: 0.9607843137, alpha: 1)
        view.layer.cornerRadius = 10
        view.clipsToBounds = true
        view.addSubview(imageView)
        
        return view
    }()
    
    // MARK: - imageView
    private lazy var imageView: UIImageView = {
        let image = UIImageView()
        image.contentMode = .scaleAspectFit
        
        return image
    }()
    
    // MARK: - nameLabel
    private lazy var nameLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "SFProDisplay-Regular", size: 14)
        label.textColor = .black
        label.adjustsFontSizeToFitWidth = true
        label.numberOfLines = 2
        
        return label
    }()
    
    // MARK: - priceLabel
    private lazy var priceLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "SFProDisplay-Regular", size: 14)
        label.textColor = .black
        label.adjustsFontSizeToFitWidth = true
        label.numberOfLines = 1
        label.textAlignment = .left
        
        return label
    }()
    
    // MARK: - weightLabel
    private lazy var weightLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "SFProDisplay-Regular", size: 14)
        label.textColor = .black
        label.alpha = 0.4
        label.adjustsFontSizeToFitWidth = true
        label.numberOfLines = 1
        
        return label
    }()
    
    // MARK: - labelsStack
    private lazy var labelsStack: UIStackView = {
        let stack = UIStackView()
        stack.axis = .horizontal
        stack.spacing = 8
        stack.alignment = .leading
        [priceLabel, weightLabel].forEach { item in
            stack.addArrangedSubview(item)
        }
        
        return stack
    }()
    
    // MARK: - containerStackView
    private lazy var containerStackView: UIStackView = {
        let stack = UIStackView()
        stack.axis = .vertical
        stack.spacing = 4
        stack.alignment = .leading
        stack.distribution = .fillEqually
        [nameLabel, labelsStack].forEach { item in
            stack.addArrangedSubview(item)
        }
        
        return stack
    }()
    
    // MARK: - counterStackView
    private lazy var counterStackView: UIStackView = {
        let stack = UIStackView()
        stack.axis = .horizontal
        stack.backgroundColor = #colorLiteral(red: 0.937254902, green: 0.9333333333, blue: 0.9254901961, alpha: 1)
        stack.layer.cornerRadius = 10
        [decreaseButton, counterLabel, increaseButton].forEach { item in
            stack.addArrangedSubview(item)
        }
        
        return stack
    }()
    
    // MARK: - counterLabel
    private lazy var counterLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "SFProDisplay-Regular", size: 14)
        label.textColor = .black
        label.adjustsFontSizeToFitWidth = true
        label.numberOfLines = 1
        label.textAlignment = .center
        
        return label
    }()
    
    // MARK: - increaseButton
    private lazy var increaseButton: UIButton = {
        var configuration = UIButton.Configuration.plain()
        let button = UIButton(configuration: configuration, primaryAction: nil)
        button.setImage(UIImage(named: "increase"), for: .normal)
        button.addTarget(self, action: #selector(increase), for: .touchUpInside)
        
        return button
    }()
    
    // MARK: - decreaseButton
    private lazy var decreaseButton: UIButton = {
        var configuration = UIButton.Configuration.plain()
        let button = UIButton(configuration: configuration, primaryAction: nil)
        button.setImage(UIImage(named: "decrease"), for: .normal)
        button.addTarget(self, action: #selector(decrease), for: .touchUpInside)
        
        return button
    }()
    
    // MARK: - Public Properties
    var changeCounterCompletion: ((ShoppingCartModel?, ShoppingCartCounterValue)->())?
    
    // MARK: - Private Properties
    private let queue = DispatchQueue(label: "queue.cell")
    private var checkData: ShoppingCartModel?
    
    // MARK: - Lifecycle
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupConstrains()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Override Methods
    override func prepareForReuse() {
        imageView.image = nil
        nameLabel.text = nil
        priceLabel.text = nil
        weightLabel.text = nil
        counterLabel.text = nil
    }
    
    // MARK: - Objc Methods
    @objc func increase() {
        changeCounterCompletion?(checkData, .increase)
    }
    
    @objc func decrease() {
        changeCounterCompletion?(checkData, .decrease)
    }
    
    // MARK: - Private Methods
    private func setupConstrains() {
        setupContainerViewConstraints()
        setupImageViewConstraints()
        setupCounterStackViewConstraints()
        setupContainerStackViewConstraints()
        setupIncreaseButtonConstraints()
        setupIncreaseButtonConstraints()
    }
    
    // MARK: - setupContainerViewConstraints
    private func setupContainerViewConstraints() {
        containerView.translatesAutoresizingMaskIntoConstraints = false
        addSubview(containerView)
        NSLayoutConstraint.activate([
            containerView.topAnchor.constraint(equalTo: topAnchor),
            containerView.leadingAnchor.constraint(equalTo: leadingAnchor),
            containerView.bottomAnchor.constraint(equalTo: bottomAnchor),
            containerView.heightAnchor.constraint(equalTo: containerView.widthAnchor, multiplier: 1)
        ])
        
    }
    
    // MARK: - setupImageViewConstraints
    private func setupImageViewConstraints() {
        imageView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            imageView.centerXAnchor.constraint(equalTo: containerView.centerXAnchor),
            imageView.centerYAnchor.constraint(equalTo: containerView.centerYAnchor),
            imageView.heightAnchor.constraint(equalTo: containerView.heightAnchor, multiplier: 0.9),
            imageView.heightAnchor.constraint(equalTo: imageView.widthAnchor, multiplier: 1)
        ])
    }
    
    // MARK: - setupCounterStackViewConstraints
    private func setupCounterStackViewConstraints() {
        counterStackView.translatesAutoresizingMaskIntoConstraints = false
        addSubview(counterStackView)
        NSLayoutConstraint.activate([
            counterStackView.centerYAnchor.constraint(equalTo: centerYAnchor),
            counterStackView.trailingAnchor.constraint(equalTo: trailingAnchor),
            counterStackView.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.29)
        ])
    }
    
    // MARK: - setupContainerStackViewConstraints
    private func setupContainerStackViewConstraints() {
        containerStackView.translatesAutoresizingMaskIntoConstraints = false
        addSubview(containerStackView)
        NSLayoutConstraint.activate([
            containerStackView.topAnchor.constraint(equalTo: topAnchor),
            containerStackView.leadingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: 8),
            containerStackView.bottomAnchor.constraint(equalTo: bottomAnchor),
            containerStackView.trailingAnchor.constraint(equalTo: counterStackView.leadingAnchor)
        ])
    }
    
    // MARK: - setupIncreaseButtonConstraints
    private func setupIncreaseButtonConstraints() {
        increaseButton.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            increaseButton.widthAnchor.constraint(equalTo: decreaseButton.widthAnchor, multiplier: 1)
        ])
    }
    
    // MARK: - setupCounterLabelConstraints
    private func setupCounterLabelConstraints() {
        counterLabel.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            counterLabel.widthAnchor.constraint(equalTo: increaseButton.widthAnchor, multiplier: 1.5)
        ])
    }
}

// MARK: - CollectionViewCellsProtocol
extension ShoppingCartInfoCell: CollectionViewCellsProtocol {
    
    typealias CellType = ShoppingCartModel
    
    static var reuseID: String {
        return "ShoppingCartSection"
    }
    
    func configure(with item: ShoppingCartModel) {
        checkData = item
        queue.async {
            guard let url = URL(string:"\(item.dish.imageURL)") else {
                print("Error with url image")
                return }
            if let data = try? Data(contentsOf: url),
               self.checkData == item
            {
                DispatchQueue.main.async {
                    self.imageView.image = UIImage(data: data)
                    self.nameLabel.text = item.dish.name
                    self.priceLabel.text = "\(item.dish.price * item.count )Р"
                    self.weightLabel.text = "\(item.dish.weight)г"
                    self.counterLabel.text = "\(item.count)"
                }
            }
        }
    }
}
