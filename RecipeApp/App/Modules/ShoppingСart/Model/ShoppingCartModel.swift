//
//  ShoppingCartModel.swift
//  RecipeApp
//
//  Created by Mac Admin on 24.08.2023.
//

struct ShoppingCartModel: Hashable {
    let dish: Dish
    var count: Int
}

