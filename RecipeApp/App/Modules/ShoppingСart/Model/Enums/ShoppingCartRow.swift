//
//  ShoppingCartRow.swift
//  RecipeApp
//
//  Created by Mac Admin on 24.08.2023.
//

enum ShoppingCartRow {
    case dishInfo(ShoppingCartModel)
}

extension ShoppingCartRow: Hashable {
    func hash(into hasher: inout Hasher) {
        switch self {
        case .dishInfo(let model):
            hasher.combine(model.dish.id)
        }
    }
    
    static func ==(lhs: ShoppingCartRow, rhs: ShoppingCartRow) -> Bool {
        switch (lhs, rhs) {
        case (.dishInfo(let model1), .dishInfo(let model2)):
            return model1.dish.id == model2.dish.id && model1.count == model2.count
        }
    }
}
