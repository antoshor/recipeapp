//
//  Enums.swift
//  RecipeApp
//
//  Created by Mac Admin on 25.09.2023.
//

enum ShoppingCartCounterValue {
    case decrease
    case increase
    
    var value: Int {
        switch self {
            
        case .decrease:
            return 1
        case .increase:
            return 1
        }
    }
}
