//
//  ShoppingCartViewController.swift
//  RecipeApp
//
//  Created by Mac Admin on 24.08.2023.
//

import UIKit

final class ShoppingCartViewController: UIViewController, ShoppingCartViewProtocol, CompletionController {
    
    // MARK: - UI Properties
    // MARK: - shoppingCartCollectionView
    private lazy var shoppingCartCollectionView: UICollectionView = {
        let collectionView = UICollectionView(frame: CGRect(x: 0, y: 0, width: 100, height: 100),collectionViewLayout: ShoppingCartCompositionalLayout.createLayout())
        collectionView.register(ShoppingCartInfoCell.self, forCellWithReuseIdentifier: ShoppingCartInfoCell.reuseID)
        
        return collectionView
    }()
    
    // MARK: - payButton
    private lazy var payButton: UIButton = {
        var configuration = UIButton.Configuration.filled()
        configuration.cornerStyle = .large
        configuration.buttonSize = .large
        configuration.baseBackgroundColor = #colorLiteral(red: 0.2, green: 0.3921568627, blue: 0.8784313725, alpha: 1)
        
        let button = UIButton(configuration: configuration, primaryAction: nil)
        button.addTarget(self, action: #selector(payButtonTapped), for: .touchUpInside)
        
        return button
    }()
    
    // MARK: - Public Properties
    var presenter: ShoppingCartPresenterProtocol?
    var collectionViewDataSource: ShoppingCartDiffableDataSourceProtocol?
    
    typealias CompletionData = [Dish:Int]
    var completionHandler: (([Dish:Int]) -> ())?
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        presenter?.initialLoad()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        presenter?.shoppingCartClosed()
    }
    
    // MARK: - Objc Methods
    @objc func locationItemTapped() {
        print("locationItemTapped")
    }
    
    @objc func profileItemTapped() {
        print("profileItemTapped")
    }
    
    @objc func payButtonTapped() {
        print("payButtonTapped")
    }
    
    // MARK: - Public Methods
    // MARK: - showUI
    func setupDataSource(with dishList: [Dish:Int]) {
        setupConstraints()
        collectionViewDataSource?.configureDataSource(collectionView: shoppingCartCollectionView, dishList: dishList)
        
        collectionViewDataSource?.increaseCompletion = { [weak self] dishItem, count in
            self?.presenter?.increaseCounter(for: dishItem, by: count)
        }

        collectionViewDataSource?.decreaseCompletion = { [weak self] dishItem, count in
            self?.presenter?.decreaseCounter(for: dishItem, by: count)
        }
    }
    
    // MARK: - updateUI
    func reloadUI(dishList: [Dish:Int], with price: String ) {
        collectionViewDataSource?.reloadData(with: dishList, withAnimation: true)
        updatePayButtonTitle(title: price)
    }
    
    // MARK: - shoppingCartInput
    func shoppingCartInput(dishList: [Dish:Int]) {
        presenter?.updateShoppingCart(dishList: dishList)
    }
    
    // MARK: - shoppingCartOutput
    func shoppingCartOutput(dishList: [Dish:Int]) {
        completionHandler?(dishList)
    }
    
    // MARK: - createLeftNavigationItem
    func createLeftNavigationItem(titleLocation: String, subtitleDate: String, imageName: String) {
        var configuration = UIButton.Configuration.plain()
        configuration.title = titleLocation
        configuration.baseForegroundColor = .black
        configuration.subtitle = subtitleDate
        configuration.image = UIImage(named: imageName)
        configuration.imagePadding = 5
        
        let button = UIButton(configuration: configuration, primaryAction: nil)
        button.addTarget(self, action: #selector(locationItemTapped), for: .touchUpInside)
        
        let leftBarButton = UIBarButtonItem.init(customView: button)
        navigationItem.leftBarButtonItem = leftBarButton
    }
    
    // MARK: - createRightNavigationItem
    func createRightNavigationItem(imageName: String) {
        let button = UIButton()
        button.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        button.layer.cornerRadius = button.frame.height / 2
        button.clipsToBounds = true
        button.setImage(UIImage(named: imageName), for: .normal)
        button.addTarget(self, action: #selector(profileItemTapped), for: .touchUpInside)
        
        let rightBarButton = UIBarButtonItem(customView: UIView(frame: CGRect(x: 0, y: 0, width: 40, height: 40)))
        rightBarButton.customView?.addSubview(button)
        rightBarButton.customView?.frame = button.frame
        
        navigationItem.rightBarButtonItem = rightBarButton
    }
    
    // MARK: - Private Methods
    // MARK: - updatePayButtonTitle
    private func updatePayButtonTitle(title: String) {
        var container = AttributeContainer()
        container.font = UIFont(name: "SFProDisplay-Medium", size: 14)
        payButton.configuration?.attributedTitle = AttributedString(title, attributes: container)
    }
    
    // MARK: - setupConstraints
    private func setupConstraints() {
        setupPayButtonConstraints()
        setupCollectionViewConstraints()
    }
    
    // MARK: - setupCollectionViewConstraints
    private func setupCollectionViewConstraints() {
        shoppingCartCollectionView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(shoppingCartCollectionView)
        
        let margins = view.layoutMarginsGuide
        NSLayoutConstraint.activate([
            shoppingCartCollectionView.topAnchor.constraint(equalTo: margins.topAnchor),
            shoppingCartCollectionView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            shoppingCartCollectionView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            shoppingCartCollectionView.bottomAnchor.constraint(equalTo: payButton.topAnchor)
        ])
    }
    
    // MARK: - setupPayButtonConstraints
    private func setupPayButtonConstraints() {
        payButton.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(payButton)
        
        let margins = view.layoutMarginsGuide
        NSLayoutConstraint.activate([
            payButton.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            payButton.leadingAnchor.constraint(equalTo: margins.leadingAnchor),
            payButton.trailingAnchor.constraint(equalTo: margins.trailingAnchor),
            payButton.bottomAnchor.constraint(equalTo: margins.bottomAnchor, constant: -16)
        ])
    }
}
