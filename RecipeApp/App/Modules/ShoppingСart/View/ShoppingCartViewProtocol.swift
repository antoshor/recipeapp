//
//  ShoppingCartViewProtocol.swift
//  RecipeApp
//
//  Created by Mac Admin on 24.08.2023.
//

protocol ShoppingCartViewProtocol: AnyObject {
    
    var collectionViewDataSource: ShoppingCartDiffableDataSourceProtocol? { get set }
    
    func shoppingCartOutput(dishList: [Dish:Int])
    
    func shoppingCartInput(dishList: [Dish:Int])
    
    func setupDataSource(with dishList: [Dish:Int])
    
    func reloadUI(dishList: [Dish:Int], with price: String ) 
   
    func createLeftNavigationItem(titleLocation: String, subtitleDate: String, imageName: String)
    
    func createRightNavigationItem(imageName: String)
}
