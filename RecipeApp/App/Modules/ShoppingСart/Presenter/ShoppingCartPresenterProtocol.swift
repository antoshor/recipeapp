//
//  ShoppingCartPresenterProtocol.swift
//  RecipeApp
//
//  Created by Mac Admin on 24.08.2023.
//

protocol ShoppingCartPresenterProtocol: AnyObject {
 
    func initialLoad()
    
    func didLoadData(data: [Dish:Int])
    
    func shoppingCartClosed()
    
    func updateShoppingCart(dishList: [Dish:Int])
    
    func reloadData(dishList: [Dish : Int])
    
    func setupNavigationVC(with: String)
    
    func increaseCounter(for item: ShoppingCartModel, by count: Int)
   
    func decreaseCounter(for item: ShoppingCartModel, by count: Int)
}
