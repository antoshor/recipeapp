//
//  ShoppingCartPresenter.swift
//  RecipeApp
//
//  Created by Mac Admin on 24.08.2023.
//

final class ShoppingCartPresenter {
    
    // MARK: - Public Properties
    weak var view: ShoppingCartViewProtocol?
    let interactor: ShoppingCartInteractorProtocol
    
    // MARK: - Initializers
    init(interactor: ShoppingCartInteractorProtocol) {
        self.interactor = interactor
    }
}

// MARK: - ShoppingCartPresenterProtocol
extension ShoppingCartPresenter: ShoppingCartPresenterProtocol {
    
    // MARK: - updateShoppingCart
    func updateShoppingCart(dishList: [Dish:Int]) {
        interactor.updateDishList(newDishList: dishList)
    }
    
    // MARK: - initialLoad
    func initialLoad() {
        interactor.loadData()
        interactor.getNowDate()
    }
    
    // MARK: - didLoadData
    func didLoadData(data: [Dish:Int]) {
        let price = updatePrice(for: data)
        view?.setupDataSource(with: data)
        view?.reloadUI(dishList: data, with: price)
    }
    
    // MARK: - reloadData
    func reloadData(dishList: [Dish : Int]) {
        let price = updatePrice(for: dishList)
        view?.reloadUI(dishList: dishList, with: price)
    }
    
    // MARK: - shoppingCartClosed
    func shoppingCartClosed() {
        let dishList = interactor.getDishList()
        view?.shoppingCartOutput(dishList: dishList)
    }
    
    // MARK: - setupNavigationVC
    func setupNavigationVC(with date: String) {
        view?.createLeftNavigationItem(titleLocation: Location.moscow.rawValue, subtitleDate: date, imageName: ImageItems.location.rawValue)
        view?.createRightNavigationItem(imageName: ImageItems.profile.rawValue)
    }
    
    // MARK: - increaseCounter
    func increaseCounter(for item: ShoppingCartModel, by count: Int) {
        interactor.increaseDishList(for: item, by: count)
    }
    
    // MARK: - decreaseCounter
    func decreaseCounter(for item: ShoppingCartModel, by count: Int) {
        interactor.decreaseDishList(for: item, by: count)
    }
    
    // MARK: - Private Methods
    private func updatePrice(for dishList: [Dish:Int]) -> String {
        var price = 0
       
        for (value, count) in dishList {
            price += value.price * count
        }
        return "Оплатить \(price) Р"
    }
}
