//
//  ShoppingCartInteractorProtocol.swift
//  RecipeApp
//
//  Created by Mac Admin on 24.08.2023.
//

protocol ShoppingCartInteractorProtocol: AnyObject {
    
    func loadData()
    
    func getNowDate()
    
    func updateDishList(newDishList: [Dish: Int])
    
    func increaseDishList(for item: ShoppingCartModel, by count: Int)
    
    func decreaseDishList(for item: ShoppingCartModel, by count: Int)
    
    func getDishList() -> [Dish:Int] 
}
