//
//  ShoppingCartInteractor.swift
//  RecipeApp
//
//  Created by Mac Admin on 24.08.2023.
//

import Foundation

final class ShoppingCartInteractor {
    
    // MARK: - Public Properties
    weak var presenter: ShoppingCartPresenterProtocol?
    
    // MARK: - Private Properties
    private var dishList = [Dish: Int]()
    
    // MARK: - Initializers
    init(dishList: [Dish: Int]) {
        self.dishList = dishList
    }
}

// MARK: - ShoppingCartInteractorProtocol
extension ShoppingCartInteractor: ShoppingCartInteractorProtocol {
    
    // MARK: - loadData
    func loadData() {
        presenter?.didLoadData(data: dishList)
    }
    
    // MARK: - updateDishList
    func updateDishList(newDishList: [Dish: Int]) {
        self.dishList = newDishList
        presenter?.reloadData(dishList: newDishList)
    }
    
    // MARK: - getNowDate
    func getNowDate() {
        let now = Date()
        let formatter = DateFormatter()
        formatter.locale = Locale(identifier: "ru_RU")
        formatter.setLocalizedDateFormatFromTemplate("dd MMMM")
        let dateString = formatter.string(from: now)
        presenter?.setupNavigationVC(with: dateString)
    }
    
    // MARK: - increaseDishList
    func increaseDishList(for item: ShoppingCartModel, by count: Int) {
        dishList[item.dish]! += 1
        presenter?.reloadData(dishList: dishList)
    }
    
    // MARK: - decreaseDishList
    func decreaseDishList(for item: ShoppingCartModel, by count: Int) {
        guard (dishList[item.dish]! - 1) > 0 else {
            dishList.removeValue(forKey: item.dish)
            presenter?.reloadData(dishList: dishList)
            return
        }
        
        dishList[item.dish]! -= count
        presenter?.reloadData(dishList: dishList)
    }
    
    // MARK: - getDishList
    func getDishList() -> [Dish:Int] {
        return dishList
    }
}
