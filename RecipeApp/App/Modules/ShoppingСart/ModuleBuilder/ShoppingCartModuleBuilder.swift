//
//  ShoppingCartModuleBuilder.swift
//  RecipeApp
//
//  Created by Mac Admin on 24.08.2023.
//

final class ShoppingCartModuleBuilder {
    
    static func build(dishList: [Dish: Int]) -> ShoppingCartViewController {
        
        let interactor = ShoppingCartInteractor(dishList: dishList)
       
        let viewController = ShoppingCartViewController()
        
        let presenter = ShoppingCartPresenter(interactor: interactor)
        presenter.view = viewController
         
        viewController.presenter = presenter
        viewController.collectionViewDataSource = ShoppingCartDiffableDataSource()
        
        interactor.presenter = presenter
        
        return viewController
    }
}
