//
//  ShoppingCartCompositionalLayout.swift
//  RecipeApp
//
//  Created by Mac Admin on 24.08.2023.
//

import UIKit

final class ShoppingCartCompositionalLayout: CompositionalLayoutProtocol {
    
    // MARK: - createLayout
    static func createLayout() -> UICollectionViewLayout {
        let layout = UICollectionViewCompositionalLayout { sectionIndex, layoutEnvironment -> NSCollectionLayoutSection? in
            
            guard let section = ShoppingCartSection(rawValue: sectionIndex) else {
                return nil
            }
            
            switch section {
            case .dish:
                return ShoppingCartCompositionalLayout.createSection()
            }
        }
        
        return layout
    }
    
    // MARK: - createSection
    private static func createSection() -> NSCollectionLayoutSection {
        let spacing = CGFloat(16)
        
        let itemSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1), heightDimension: .fractionalHeight(1))
        let item = NSCollectionLayoutItem(layoutSize: itemSize)
        
        let groupSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1), heightDimension: .fractionalWidth(0.181))
        let group = NSCollectionLayoutGroup.horizontal(layoutSize: groupSize, subitems: [item])
        
        let section = NSCollectionLayoutSection(group: group)
        section.interGroupSpacing = 16
        section.contentInsets = .init(top: spacing, leading: spacing, bottom: spacing, trailing: spacing)
        
        return section
    }
}
