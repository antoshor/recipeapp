//
//  ShoppingCartDiffableDataSourceProtocol.swift
//  RecipeApp
//
//  Created by Mac Admin on 03.10.2023.
//

import Foundation
import UIKit

protocol ShoppingCartDiffableDataSourceProtocol: DiffableDataSourceProtocol {
    
    func configureDataSource(collectionView: UICollectionView, dishList: [Dish:Int])
    
    func reloadData(with data: [Dish:Int], withAnimation: Bool)
    
    var increaseCompletion: ((ShoppingCartModel, Int)-> ())? { get set }
    var decreaseCompletion: ((ShoppingCartModel, Int)-> ())? { get set }
}
