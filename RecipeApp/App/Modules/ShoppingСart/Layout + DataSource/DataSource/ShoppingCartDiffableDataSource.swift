//
//  ShoppingCartDiffableDataSource.swift
//  RecipeApp
//
//  Created by Mac Admin on 24.08.2023.
//

import UIKit

final class ShoppingCartDiffableDataSource {
    
    // MARK: - Public Properties
    var increaseCompletion: ((ShoppingCartModel, Int)-> ())?
    var decreaseCompletion: ((ShoppingCartModel, Int)-> ())?
    
    // MARK: - Private Properties
    private var dataSource: UICollectionViewDiffableDataSource<ShoppingCartSection, ShoppingCartRow>?
    private var dishList = [Dish:Int]()
}

// MARK: - ShoppingCartDiffableDataSourceProtocol
extension ShoppingCartDiffableDataSource: ShoppingCartDiffableDataSourceProtocol {
    
    // MARK: - configureDataSource
    func configureDataSource(collectionView: UICollectionView, dishList: [Dish:Int]) {
        dataSource = UICollectionViewDiffableDataSource<ShoppingCartSection, ShoppingCartRow>(collectionView: collectionView, cellProvider: { (collectionView, indexPath, item) -> UICollectionViewCell? in
            
            switch item {
            case .dishInfo(let info):
                let cell = self.createCell(cellType: ShoppingCartInfoCell.self, indexPath: indexPath, collectionView: collectionView)
                
                cell.configure(with: info)
                
                cell.changeCounterCompletion = { dishItem, count in
                    guard let dishItem = dishItem else {
                        return
                    }
                    
                    switch count {
                    case .decrease:
                        self.decreaseCompletion?(dishItem, count.value)
                    case .increase:
                        self.increaseCompletion?(dishItem, count.value)
                    }
                }
                return cell
            }
        })
    }
    
    // MARK: - reloadData
    func reloadData(with data: [Dish:Int], withAnimation: Bool) {
        self.dishList = data
        
        var snapshot = NSDiffableDataSourceSnapshot<ShoppingCartSection, ShoppingCartRow>()
        
        snapshot.appendSections([.dish])
        
        for (key, value) in data {
            let dish = ShoppingCartRow.dishInfo(ShoppingCartModel(dish: key, count: value))
            snapshot.appendItems([dish], toSection: .dish)
        }
        
        dataSource?.apply(snapshot, animatingDifferences: withAnimation)
    }
}

