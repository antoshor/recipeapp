//
//  CoordinatorProtocol.swift
//  RecipeApp
//
//  Created by Mac Admin on 01.09.2023.
//

typealias CompletionHandler = ((ShoppingCartModel)->())

// MARK: - CoordinatorProtocol
protocol CoordinatorProtocol: AnyObject {
    
    func start()
}

// MARK: - TabBarCoordinatorProtocol
protocol TabBarCoordinatorProtocol: CoordinatorProtocol {}

// MARK: - SelectFoodCoordinatorProtocol
protocol SelectFoodCoordinatorProtocol: CoordinatorProtocol {
    
    var outputToTabBarCoordinatorCompletionHandler: CompletionHandler? { get set }
    
    var backToTabBarCoordinatorCompletionHandler: (()->())? { get set }
}
