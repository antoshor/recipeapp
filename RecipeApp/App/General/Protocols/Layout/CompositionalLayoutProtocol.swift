//
//  CompositionalLayoutProtocol.swift
//  RecipeApp
//
//  Created by Mac Admin on 03.10.2023.
//

import UIKit

protocol CompositionalLayoutProtocol {
    static func createLayout() -> UICollectionViewLayout
}

