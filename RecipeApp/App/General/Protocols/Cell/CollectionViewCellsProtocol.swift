//
//  CollectionViewCellsProtocol.swift
//  RecipeApp
//
//  Created by Mac Admin on 10.08.2023.
//

protocol CollectionViewCellsProtocol {
    associatedtype CellType
    
    static var reuseID: String { get }
    
    func configure(with: CellType)
}



