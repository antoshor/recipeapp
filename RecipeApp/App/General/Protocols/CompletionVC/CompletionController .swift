//
//  CompletionController .swift
//  RecipeApp
//
//  Created by Mac Admin on 01.09.2023.
//

protocol CompletionController: AnyObject {
    
    associatedtype CompletionData
    
    var completionHandler: ((CompletionData)->())? { get set }
}
