//
//  DataSource.swift
//  RecipeApp
//
//  Created by Mac Admin on 24.08.2023.
//

import UIKit

protocol DiffableDataSourceProtocol {
    func createCell<T: CollectionViewCellsProtocol>(cellType: T.Type, indexPath: IndexPath, collectionView: UICollectionView) -> T
}

extension DiffableDataSourceProtocol {
    func createCell<T: CollectionViewCellsProtocol>(cellType: T.Type, indexPath: IndexPath, collectionView: UICollectionView) -> T {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellType.reuseID, for: indexPath) as? T else {
            fatalError("Error \(cellType)")
        }
        
        return cell
    }
}
