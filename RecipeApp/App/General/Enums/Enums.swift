//
//  Enums.swift
//  RecipeApp
//
//  Created by Mac Admin on 04.10.2023.
//

// MARK: - Location
enum Location: String {
    case moscow = "Москва"
}

// MARK: - ImageItems
enum ImageItems: String {
    case location = "locationItem"
    case profile = "profileItem"
}

// MARK: - API
enum Api: String {
    case main = "https://run.mocky.io/v3/ccdd5ac3-8d40-4447-8ee6-e9e0fc84a22d"
    case categories = "https://run.mocky.io/v3/6c70893f-c6e6-41b8-bb0c-1dbe0978a92b"
}

// MARK: - TabBarItem
enum TabBarItem: Int, CaseIterable {
    
    case main
    case shoppingCart
    
    var title: String {
        switch self {
        case .main:
            return "Главная"
        case .shoppingCart:
            return "Корзина"
        }
    }
    
    var iconName: String {
        switch self {
        case .main:
            return "main"
        case .shoppingCart:
            return "shoppingCart"
        }
    }
}
