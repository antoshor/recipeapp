//
//  MainNetworkService.swift
//  RecipeApp
//
//  Created by Mac Admin on 04.07.2023.
//

import Foundation

final class NetworkService {
    
    static let shared = NetworkService()
    
    func loadData<T: Codable>(url: String, completion: @escaping (T)->()) {
        
        guard let url = URL(string: url) else { return }
        
        URLSession.shared.dataTask(with: url) { data, response, error in
            if let error = error {
                print(error.localizedDescription)
            } else if let data = data {
                guard let jsonData = try? JSONDecoder().decode(T.self, from: data) else {
                    print("Error JSONDecoder()")
                    return }
                DispatchQueue.main.async {
                    completion(jsonData)
                }
            }
        }.resume()
    }
}
