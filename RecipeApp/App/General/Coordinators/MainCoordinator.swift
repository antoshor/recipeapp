//
//  MainCoordinator.swift
//  RecipeApp
//
//  Created by Mac Admin on 01.09.2023.
//

final class MainCoordinator: CoordinatorProtocol {
    
    // MARK: - Public Properties
    var tabBarController: MainTabBarController
  
    // MARK: - Private Properties
    private var coordinators: [CoordinatorProtocol] = []
    
    // MARK: - Initializers
    init(tabBarController: MainTabBarController) {
        self.tabBarController = tabBarController
    }
    
    // MARK: - Public Methods
    func start() {
        showTabBarFlow()
    }
    
    // MARK: - Private Methods
    private func showTabBarFlow() {
        let tabBarFlow = TabBarFlowCoordinator(tabBarController: tabBarController)
        
        coordinators.append(tabBarFlow)
        tabBarFlow.start()
    }
}
