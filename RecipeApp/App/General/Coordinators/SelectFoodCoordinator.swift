//
//  SelectFoodCoordinator.swift
//  RecipeApp
//
//  Created by Mac Admin on 01.09.2023.
//

import UIKit

final class SelectFoodCoordinator {
    
    // MARK: - Public Properties
    var outputToTabBarCoordinatorCompletionHandler: CompletionHandler?
    var backToTabBarCoordinatorCompletionHandler: (()->())?

    // MARK: - Private Properties
    private var navigationController: UINavigationController
    private let title: String

    // MARK: - Initializers
    init(navigationController: UINavigationController, categoryTitle: String) {
        self.title = categoryTitle
        self.navigationController = navigationController
    }
    
    // MARK: - Private Methods
    // MARK: - showCategoriesModule()
    private func showCategoriesModule() {
        let categoriesModule = CategoriesModuleBuilder.build(categoryTitle: title)
        
        categoriesModule.completionHandler = { [weak categoriesModule] dish in
            self.showDetailModule(view: categoriesModule, dish: dish)
        }
        
        categoriesModule.completionHandlerBackButtonPressed = { [weak self] in
            self?.backToTabBarCoordinatorCompletionHandler?()
        }
        
        navigationController.pushViewController(categoriesModule, animated: true)
    }
    
    // MARK: - showDetailModule()
    private func showDetailModule(view: CategoriesViewController?, dish: Dish) {
        let detailModule = DetailModuleBuilder.build(data: dish)
        
        detailModule.completionHandler = { [weak self, weak detailModule] dishItem in
            if let dishItem = dishItem {
               
                self?.outputToTabBarCoordinatorCompletionHandler?(dishItem)
                detailModule?.dismiss(animated: true)
                print("Добавлено в заказ")
                
            } else {
                detailModule?.dismiss(animated: true)
                print("Отмена добавления")
            }
        }
        
        detailModule.modalPresentationStyle = .overFullScreen
        detailModule.modalTransitionStyle = .crossDissolve
        
        view?.present(detailModule, animated: true)
    }
}

// MARK: - SelectFoodCoordinatorProtocol
extension SelectFoodCoordinator: SelectFoodCoordinatorProtocol {
    func start() {
        showCategoriesModule()
    }
}
