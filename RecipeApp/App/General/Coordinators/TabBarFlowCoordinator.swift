//
//  TabBarFlowCoordinator.swift
//  RecipeApp
//
//  Created by Mac Admin on 02.09.2023.
//

import UIKit

final class TabBarFlowCoordinator: TabBarCoordinatorProtocol {
    
    // MARK: - Private Properties
    private var mainNavigationController: UINavigationController?
    private var shoppingCartNavigationController: UINavigationController?
    
    private var tabBarController: MainTabBarController?
    
    private var shoppingCartModule: ShoppingCartViewProtocol?
    private var mainModule: MainViewControllerProtocol?
    
    private var dishList = [Dish:Int]()
    private var coordinators: [CoordinatorProtocol]?
    
    // MARK: - Initializers
    init(tabBarController: MainTabBarController) {
        self.tabBarController = tabBarController
    }
    
    // MARK: - Public Methods
    func start() {
        createTabBarModule()
    }
    
    // MARK: - Private Methods
    // MARK: - createTabBarModule
    private func createTabBarModule() {
        shoppingCartModule = createShoppingCartModule()
        mainModule = createMainModule()
        
        mainNavigationController = UINavigationController(rootViewController: mainModule as! UIViewController)

        shoppingCartNavigationController = UINavigationController(rootViewController: shoppingCartModule as! UIViewController)
        
        tabBarController?.setupTabBar(views: [mainNavigationController, shoppingCartNavigationController])
    }
    
    // MARK: - createShoppingCartModule
    private func createShoppingCartModule() -> ShoppingCartViewController  {
        let shoppingCartModule = ShoppingCartModuleBuilder.build(dishList: dishList)
        
        shoppingCartModule.completionHandler = { [weak self] dishList in
            self?.dishList = dishList
        }
        
        return shoppingCartModule
    }
    
    // MARK: - createMainModule
    private func createMainModule() -> MainViewController {
        let mainModule = MainModuleBuilder.build()
        
        mainModule.completionHandler = { [weak self] title in
            self?.createSelectFoodFlow(title: title)
        }
    
        return mainModule
    }
    
    // MARK: - createSelectFoodFlow
    private func createSelectFoodFlow(title: String) {
        let selectFoodFlow = SelectFoodCoordinator(navigationController: mainNavigationController!, categoryTitle: title)
        
        coordinators?.append(selectFoodFlow)
        
        selectFoodFlow.outputToTabBarCoordinatorCompletionHandler = { [weak self] dishItem in
            guard let self = self else {
                return
            }
            
            if let oldValue = self.dishList.updateValue(dishItem.count, forKey: dishItem.dish) {
                self.dishList[dishItem.dish] = oldValue + dishItem.count
            }
            
            self.shoppingCartModule?.shoppingCartInput(dishList: self.dishList)
        }
        
        selectFoodFlow.backToTabBarCoordinatorCompletionHandler = { [weak self] in
            self?.coordinators?.removeAll()
        }
        
        selectFoodFlow.start()
    }
}


