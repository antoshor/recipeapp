//
//  MainTabBarController.swift
//  RecipeApp
//
//  Created by Mac Admin on 05.07.2023.
//

import UIKit

final class MainTabBarController: UITabBarController {
    
    // MARK: - Initializers
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    // MARK: - Public Properties
    func setupTabBar(views: [UINavigationController?]) {
        for (index, viewController) in views.enumerated() {
            guard let tabBarItem = TabBarItem(rawValue: index) else {
                return
            }
            
            guard let viewController = viewController else {
                return
            }
            
            viewController.tabBarItem.title = tabBarItem.title
            viewController.tabBarItem.image = UIImage(named: tabBarItem.iconName)
            viewControllers = (viewControllers ?? []) + [viewController]
        }
    }
}
